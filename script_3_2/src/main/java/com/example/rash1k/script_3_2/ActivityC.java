package com.example.rash1k.script_3_2;

import android.content.Intent;
import android.view.View;

public class ActivityC extends ActivityA {


    @Override
    public void nextActivity(View view) {
        Intent nextActivity = new Intent(this, ActivityD.class);
        startActivity(nextActivity);
    }
}
