package com.example.rash1k.script_3_2;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.rash1k.stack.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class ActivityA extends AppCompatActivity {

    private TextView mTextViewInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);

        mTextViewInfo = (TextView) findViewById(R.id.tv_info);

        initToolbar();
        showDirectStack();
        Log.d(getLocalClassName(), " onCreate");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(getLocalClassName(), "onRestart: ");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(getLocalClassName(), " onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(getLocalClassName(), " onResume: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(getLocalClassName(), " onPause: ");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(getLocalClassName(), " onStop: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(getLocalClassName(), " onDestroy: ");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            if(getClass().getSimpleName().equals("ActivityB")){

            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void nextActivity(View view) {
        Intent nextActivity = new Intent(this, ActivityB.class);
        nextActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(nextActivity);

        Log.d(getLocalClassName(), "nextActivity B: ");
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        if (!getClass().getSimpleName().equals("ActivityA")) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        Log.d(getLocalClassName(), getClass().getSimpleName());
    }

    private void showDirectStack() {
        String textNameActivity = null;

        switch (getClass().getSimpleName()) {

            case "ActivityA":
                textNameActivity = "A";
                break;
            case "ActivityB":
                textNameActivity = "A -> B";
                break;
            case "ActivityC":
                textNameActivity = "B -> C";
                break;
            case "ActivityD":
                textNameActivity = "C -> D";
                break;
        }

        mTextViewInfo.setText(textNameActivity);
    }

    @NonNull
    private CharSequence readLogcat() throws IOException {
        Process process = Runtime.getRuntime().exec("logcat -d");

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

        StringBuilder log = new StringBuilder();

        String line = null;

        while ((line = reader.readLine()) != null) {

            String match = "on";

            if (line.startsWith(match)) {
                log.append(line.substring(line.indexOf(match), line.length()));
                log.append("\n");
            }

        }
        return log.toString();
    }

    protected void showTaskInfo() {
        final int MAX_NUM_TASK = 10;

        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> appTasks = activityManager.getRunningTasks(MAX_NUM_TASK);

        for (ActivityManager.RunningTaskInfo taskInfo : appTasks) {
            if (taskInfo.baseActivity.flattenToShortString().startsWith("com.example.rash1k")) {
                String baseActivity = taskInfo.baseActivity.toShortString();
                String topActivity = taskInfo.topActivity.toShortString();

                Log.i(getLocalClassName(), " CountActivities:" + taskInfo.numActivities);
                Log.i(getLocalClassName(), " BaseActivity:" + baseActivity);
                Log.i(getLocalClassName(), " TopActivity:" + topActivity);
                Log.i(getLocalClassName(), "-----------------");
            }
        }
    }

    public void info(View view) {
        showTaskInfo();
    }
}