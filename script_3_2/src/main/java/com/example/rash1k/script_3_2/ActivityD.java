package com.example.rash1k.script_3_2;

import android.content.Intent;
import android.view.View;

public class ActivityD extends ActivityA {

    @Override
    public void nextActivity(View view) {
        Intent nextActivity = new Intent(this, ActivityA.class);
        nextActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(nextActivity);
    }
}
