package com.example.rash1k.activitieslaunchmode;

import android.content.Intent;
import android.view.View;

public class ActivityC extends BaseActivity {

    private static final String LOG_TAG = ActivityC.class.getSimpleName();



    @Override
    public void nextActivity(View view) {
        Intent nextActivity = new Intent(this, ActivityD.class);
        startActivityForResult(nextActivity,0);
}
/*
    @Override
    public void onBackPressed() {

        String data = "C -> B";

        Intent intent = new Intent();
        intent.putExtra(Intent.EXTRA_TEXT,data);

        setResult(RESULT_OK,intent);

        super.onBackPressed();
    }*/

}
