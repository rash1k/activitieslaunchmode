package com.example.rash1k.activitieslaunchmode;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

public abstract class BaseActivity extends AppCompatActivity {

    private TextView mTvInfoStack;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(getLocalClassName(), " onNewIntent: ");
        mTvInfoStack.setText(intent.getExtras().getString(Intent.EXTRA_TEXT));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
        initToolbar();

        String textNameActivity = null;

        mTvInfoStack = (TextView) findViewById(R.id.tv_info);

        switch (getLocalClassName()) {
            case "ActivityA":
               textNameActivity = "A";
                break;
            case "ActivityB":
                textNameActivity = "A -> B";
                break;
            case "ActivityC":
                textNameActivity = "B -> C";
                break;
            case "ActivityD":
                textNameActivity = "C -> D";
                break;
//            }
        }
        Log.d(getLocalClassName(), " onCreate: ");
        mTvInfoStack.setText(textNameActivity);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            deliveryResultToHomeUpPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(getLocalClassName(), "onRestart: ");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(getLocalClassName(), " onStart");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(getLocalClassName(), " onResume: ");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(getLocalClassName(), "onPause ");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        outState.putString(NAME_ACTIVITY_KEY, textNameActivity);
        super.onSaveInstanceState(outState);
        Log.d(getLocalClassName(), "onSaveInstanceState: ");

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        textNameActivity = savedInstanceState.getString(NAME_ACTIVITY_KEY);
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(getLocalClassName(), "onRestoreInstanceState: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(getLocalClassName(), " onStop: ");

    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
        Log.d(getLocalClassName(), " onDestroy: ");

    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        if (!getLocalClassName().equals("ActivityA")) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void deliveryResultToHomeUpPressed() {
        String data = null;
        switch (getLocalClassName()) {
            case "ActivityB":
                data = "B -> A";
                break;
            case "ActivityC":
                data = "C -> B";
                break;
            case "ActivityD":
                data = "D -> B";
                break;
        }

        Intent upIntent = NavUtils.getParentActivityIntent(this);
        upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        upIntent.putExtra(Intent.EXTRA_TEXT, data);
        NavUtils.navigateUpTo(this, upIntent);
    }

/*    public void nextActivity(View view) throws ClassNotFoundException {

        String[] classNameText = {".ActivityA",".ActivityB",".ActivityC",".ActivityD"};
//        String[] labelActivity = {"A", "B", "C", "D"};
        currentActivityPosition = (currentActivityPosition + 1) % classNameText.length;

        String className = getPackageName() + classNameText[currentActivityPosition];

        Intent nextActivity = new Intent(this, Class.forName(className));
        startActivity(nextActivity);
    }*/

    protected void showTaskInfo() {
        final int MAX_NUM_TASK = 5;

        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> appTasks = activityManager.getRunningTasks(MAX_NUM_TASK);

        for (ActivityManager.RunningTaskInfo taskInfo : appTasks) {
            if (taskInfo.baseActivity.flattenToShortString().startsWith("com.example.rash1k")) {
                String baseActivity = taskInfo.baseActivity.toShortString();
                String topActivity = taskInfo.topActivity.toShortString();

                Log.i(getLocalClassName(), " CountActivities:" + taskInfo.numActivities);
                Log.i(getLocalClassName(), " BaseActivity:" + baseActivity);
                Log.i(getLocalClassName(), " TopActivity:" + topActivity);
                Log.i(getLocalClassName(), "-----------------");
            }
        }
    }

    public abstract void nextActivity(View view);

}
