package com.example.rash1k.activitieslaunchmode;

import android.content.Intent;
import android.view.View;

public class ActivityA extends BaseActivity {

    private static final String LOG_TAG = ActivityA.class.getSimpleName();


    @Override
    public void nextActivity(View view) {
        Intent nextActivity = new Intent(this, ActivityB.class);
        startActivity(nextActivity);
    }

}
