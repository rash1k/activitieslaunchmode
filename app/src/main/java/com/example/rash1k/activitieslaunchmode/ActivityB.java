package com.example.rash1k.activitieslaunchmode;

import android.content.Intent;
import android.view.View;

public class ActivityB extends BaseActivity {

    private static final String LOG_TAG = ActivityB.class.getSimpleName();



    @Override
    public void nextActivity(View view) {
        Intent nextActivity = new Intent(this, ActivityC.class);
        startActivityForResult(nextActivity,0);
    }
}
